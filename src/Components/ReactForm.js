import {Container} from "reactstrap";
import {Col , Row ,Input ,Label , Button } from "reactstrap";

const ReactForm = () =>{
    return(
        <Container>
            {/* Dòng 1 */}
            <Row className="mt-3 form-group">
                <Col sm={12}>
                    <h5 className="text-center">Registration form</h5>
                </Col>
            </Row>
            {/* Dòng 2 */}
            <Row className="mt-3 form-group">
                <Col sm={6}>
                    <Row className="form-group mt-3">
                        <Col sm={4}>
                            <Label>First name (*)</Label>
                        </Col>
                        <Col sm={8}>
                            <Input className="form-control"></Input>
                        </Col>
                    </Row>
                    <Row className="form-group mt-3">
                        <Col sm={4}>
                            <Label>Last Name (*)</Label>
                        </Col>
                        <Col sm={8}>
                            <Input className="form-control"></Input>
                        </Col>
                    </Row>
                    <Row className="form-group mt-3">
                        <Col sm={4}>
                            <Label>Birthday (*)</Label>
                        </Col>
                        <Col sm={8}>
                            <Input className="form-control"></Input>
                        </Col>
                    </Row>
                </Col>
                <Col sm={6}>
                    <Row className="form-group mt-3">
                        <Col sm={4}>
                            <Label>Passport (*)</Label>
                        </Col>
                        <Col sm={8}>
                            <Input className="form-control"></Input>
                        </Col>
                    </Row>
                    <Row className="form-group mt-3">
                        <Col sm={4}>
                            <Label>Email </Label>
                        </Col>
                        <Col sm={8}>
                            <Input className="form-control"></Input>
                        </Col>
                    </Row>
                    <Row className="form-group mt-3">
                        <Col sm={4}>
                            <Label>Country (*)</Label>
                        </Col>
                        <Col sm={8}>
                            <Input className="form-control"></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            {/* Dòng 3 */}
            <Row className="mt-3 form-group">
                <Col sm={6}>
                    <Row>
                        <Col sm={4}>
                            <Label>Gender (*)</Label>
                        </Col>
                        <Col sm={8}>
                            <Row>
                                <Col sm={6}>
                                    <Input type="radio"></Input>
                                    <Label style={{marginLeft : "10px"}}>Male</Label>
                                </Col>
                                <Col sm={6}>
                                    <Input type="radio"></Input>
                                    <Label style={{marginLeft : "10px"}}>Female</Label>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col sm={6}>
                    <Row className="form-group">
                        <Col sm={4}>
                            <Label>Region</Label>
                        </Col>
                        <Col sm={8}>
                            <Input className="form-control"></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="form-group mt-3">
                <Col sm={2}>
                    <Label>Subject</Label>
                </Col>
                <Col sm={10}>
                    <Input type="textarea" className="form-control"></Input>
                </Col>
            </Row>
            <Row className="form-group mt-3">
                <Col sm={12} style={{paddingLeft:"900px"}}>
                    <Button style={{marginRight:"10px"}} className="btn-success">Check Data</Button>
                    <Button className="btn-success">Send</Button>
                </Col>
            </Row>
        </Container>
    )
}

export default ReactForm