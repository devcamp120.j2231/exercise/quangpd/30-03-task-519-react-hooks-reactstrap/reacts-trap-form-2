import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {Container} from "reactstrap";
import ReactForm from './Components/ReactForm';
function App() {
  return (
   <Container>
      <ReactForm/>
   </Container>
  );
}

export default App;
